/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author niko9191
 */
public class Asignaturas {
    
    public int CodigoOferta ;
    public String NombreOferta;
    public float CupoOferta;
    public int Teoria;
    public int Ejercicio;
    public int Laboratorio;
    public String Seccion;
    public int HorasSemanales;

    public Asignaturas(int CodigoOferta, String NombreOferta, float CupoOferta, int Teoria, int Ejercicio, int Laboratorio, String Seccion, int HorasSemanales) {
        this.CodigoOferta = CodigoOferta;
        this.NombreOferta = NombreOferta;
        this.CupoOferta = CupoOferta;
        this.Teoria = Teoria;
        this.Ejercicio = Ejercicio;
        this.Laboratorio = Laboratorio;
        this.Seccion = Seccion;
        this.HorasSemanales = HorasSemanales;
    }

    public int getOferta() {
        return CodigoOferta;
    }

    public void setOferta(int oferta) {
        this.CodigoOferta = oferta;
    }

    public String getNombreOferta() {
        return NombreOferta;
    }

    public void setNombreOferta(String NombreOferta) {
        this.NombreOferta = NombreOferta;
    }

    public float getCupoOferta() {
        return CupoOferta;
    }

    public void setCupoOferta(float CupoOferta) {
        this.CupoOferta = CupoOferta;
    }

    public int getTeoria() {
        return Teoria;
    }

    public void setTeoria(int Teoria) {
        this.Teoria = Teoria;
    }

    public int getEjercicio() {
        return Ejercicio;
    }

    public void setEjercicio(int Ejercicio) {
        this.Ejercicio = Ejercicio;
    }

    public int getLaboratorio() {
        return Laboratorio;
    }

    public void setLaboratorio(int Laboratorio) {
        this.Laboratorio = Laboratorio;
    }

    public String getSeccion() {
        return Seccion;
    }

    public void setSeccion(String Seccion) {
        this.Seccion = Seccion;
    }

    public int getHorasSemanales() {
        return HorasSemanales;
    }

    public void setHorasSemanales(int HorasSemanales) {
        this.HorasSemanales = HorasSemanales;
    }
    
}
