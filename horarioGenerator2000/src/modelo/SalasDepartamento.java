/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author niko9191
 */
public class SalasDepartamento {
    
    public String TipoEspacio;
    public String Departamento;
    public int Identificador;
    public String CodigoFisico;
    public String NombreDelEspacio;
    public String Edificio;
    public float MetrosConstruidos;
    public int CapacidadTotalDePersonas;
    public int NumeroDePizarras;
    public int NumeroDatashow;
    public String CasillaFDI;

    public String getTipoEspacio() {
        return TipoEspacio;
    }

    public void setTipoEspacio(String TipoEspacio) {
        this.TipoEspacio = TipoEspacio;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public void setDepartamento(String Departamento) {
        this.Departamento = Departamento;
    }

    public int getIdentificador() {
        return Identificador;
    }

    public void setIdentificador(int Identificador) {
        this.Identificador = Identificador;
    }

    public String getCodigoFisico() {
        return CodigoFisico;
    }

    public void setCodigoFisico(String CodigoFisico) {
        this.CodigoFisico = CodigoFisico;
    }

    public String getNombreDelEspacio() {
        return NombreDelEspacio;
    }

    public void setNombreDelEspacio(String NombreDelEspacio) {
        this.NombreDelEspacio = NombreDelEspacio;
    }

    public String getEdificio() {
        return Edificio;
    }

    public void setEdificio(String Edificio) {
        this.Edificio = Edificio;
    }

    public float getMetrosConstruidos() {
        return MetrosConstruidos;
    }

    public void setMetrosConstruidos(float MetrosConstruidos) {
        this.MetrosConstruidos = MetrosConstruidos;
    }

    public int getCapacidadTotalDePersonas() {
        return CapacidadTotalDePersonas;
    }

    public void setCapacidadTotalDePersonas(int CapacidadTotalDePersonas) {
        this.CapacidadTotalDePersonas = CapacidadTotalDePersonas;
    }

    public int getNumeroDePizarras() {
        return NumeroDePizarras;
    }

    public void setNumeroDePizarras(int NumeroDePizarras) {
        this.NumeroDePizarras = NumeroDePizarras;
    }

    public int getNumeroDatashow() {
        return NumeroDatashow;
    }

    public void setNumeroDatashow(int NumeroDatashow) {
        this.NumeroDatashow = NumeroDatashow;
    }

    public String getCasillaFDI() {
        return CasillaFDI;
    }

    public void setCasillaFDI(String CasillaFDI) {
        this.CasillaFDI = CasillaFDI;
    }

    public SalasDepartamento(String TipoEspacio, String Departamento, int Identificador, String CodigoFisico, String NombreDelEspacio, String Edificio, float MetrosConstruidos, int CapacidadTotalDePersonas, int NumeroDePizarras, int NumeroDatashow, String CasillaFDI) {
        this.TipoEspacio = TipoEspacio;
        this.Departamento = Departamento;
        this.Identificador = Identificador;
        this.CodigoFisico = CodigoFisico;
        this.NombreDelEspacio = NombreDelEspacio;
        this.Edificio = Edificio;
        this.MetrosConstruidos = MetrosConstruidos;
        this.CapacidadTotalDePersonas = CapacidadTotalDePersonas;
        this.NumeroDePizarras = NumeroDePizarras;
        this.NumeroDatashow = NumeroDatashow;
        this.CasillaFDI = CasillaFDI;
    }
    
}
