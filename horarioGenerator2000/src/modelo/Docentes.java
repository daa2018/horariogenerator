/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author niko9191
 */
public class Docentes {
    
    public float CodigoDocente;
    public String ApellidoPaterno;
    public String ApellidoMaterno;
    public String Nombres;
    public String NombreOferta;
    public float CodigoOferta;
    public int MaximoHorasDiarias;
    public int Prioridad;
    public int MaximoHorasSemanales;

    public Docentes(float CodigoDocente, String ApellidoPaterno, String ApellidoMaterno, String Nombres, String NombreOferta, float CodigoOferta, int MaximoHorasDiarias, int Prioridad, int MaximoHorasSemanales) {
        this.CodigoDocente = CodigoDocente;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.Nombres = Nombres;
        this.NombreOferta = NombreOferta;
        this.CodigoOferta = CodigoOferta;
        this.MaximoHorasDiarias = MaximoHorasDiarias;
        this.Prioridad = Prioridad;
        this.MaximoHorasSemanales = MaximoHorasSemanales;
    }

    public float getCodigoDocente() {
        return CodigoDocente;
    }

    public void setCodigoDocente(float CodigoDocente) {
        this.CodigoDocente = CodigoDocente;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String ApellidoPaterno) {
        this.ApellidoPaterno = ApellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String ApellidoMaterno) {
        this.ApellidoMaterno = ApellidoMaterno;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getNombreOferta() {
        return NombreOferta;
    }

    public void setNombreOferta(String NombreOferta) {
        this.NombreOferta = NombreOferta;
    }

    public float getCodigoOferta() {
        return CodigoOferta;
    }

    public void setCodigoOferta(float CodigoOferta) {
        this.CodigoOferta = CodigoOferta;
    }

    public int getMaximoHorasDiarias() {
        return MaximoHorasDiarias;
    }

    public void setMaximoHorasDiarias(int MaximoHorasDiarias) {
        this.MaximoHorasDiarias = MaximoHorasDiarias;
    }

    public int getPrioridad() {
        return Prioridad;
    }

    public void setPrioridad(int Prioridad) {
        this.Prioridad = Prioridad;
    }

    public int getMaximoHorasSemanales() {
        return MaximoHorasSemanales;
    }

    public void setMaximoHorasSemanales(int MaximoHorasSemanales) {
        this.MaximoHorasSemanales = MaximoHorasSemanales;
    }
    
    
  
    
}
