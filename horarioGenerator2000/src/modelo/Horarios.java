/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author niko9191gerardo
 */
public class Horarios {
    
    public int Dia;
    public int Bloque;
    public float HoraInicio;//aqui no se si ponerle float o string , porque el formato es 08:00 , pro podria ser 8,00
    public float HoraTermino;

    public Horarios(int Dia, int Bloque, float HoraInicio, float HoraTermino) {
        this.Dia = Dia;
        this.Bloque = Bloque;
        this.HoraInicio = HoraInicio;
        this.HoraTermino = HoraTermino;
    }

    public int getDia() {
        return Dia;
    }

    public void setDia(int Dia) {
        this.Dia = Dia;
    }

    public int getBloque() {
        return Bloque;
    }

    public void setBloque(int Bloque) {
        this.Bloque = Bloque;
    }

    public float getHoraInicio() {
        return HoraInicio;
    }

    public void setHoraInicio(float HoraInicio) {
        this.HoraInicio = HoraInicio;
    }

    public float getHoraTermino() {
        return HoraTermino;
    }

    public void setHoraTermino(float HoraTermino) {
        this.HoraTermino = HoraTermino;
    }
    
    
    
}
